package com.ydl.elasticsearch.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.ydl.elasticsearch.entity.CarEla;

public interface  CarRepository extends ElasticsearchRepository<CarEla,Long>{
}
