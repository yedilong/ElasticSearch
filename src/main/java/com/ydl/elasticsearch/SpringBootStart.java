package com.ydl.elasticsearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@SpringBootApplication
@EnableTransactionManagement
public class SpringBootStart {
	public static void main(String[] args) {
		SpringApplication.run(SpringBootStart.class);
	}
}
