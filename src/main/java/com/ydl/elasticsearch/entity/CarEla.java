package com.ydl.elasticsearch.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Document(indexName = "car",type = "doc", shards = 1, replicas = 0)
public class CarEla {
	@Id 
	private Long id;
	@Field(type = FieldType.Text , analyzer = "ik_max_word")
	private String brand;//品牌
	@Field(type = FieldType.Text , analyzer = "ik_max_word")
	private String model;//车型
	@Field(type = FieldType.Text , analyzer = "ik_max_word")
	private String colour;//颜色
	
	public CarEla() {
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getColour() {
		return colour;
	}
	public void setColour(String colour) {
		this.colour = colour;
	}
}

