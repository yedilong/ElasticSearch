package com.ydl.elasticsearch.repository;

import java.util.ArrayList;
import java.util.List;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.ydl.elasticsearch.entity.CarEla;
 

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class CarRepositoryTest {

//	@Autowired
//	private ElasticsearchTemplate elasticsearchTemplate;

	@Autowired
	private CarRepository carRepository;

	@Test
	public void searchCar() {
		BoolQueryBuilder builder = QueryBuilders.boolQuery();
		String key = "白色大众";
		for (String str : getKeysName()) {
			builder.should(QueryBuilders.matchQuery(str, key));
		}
		// 构建查询条件
		NativeSearchQueryBuilder queryBuilder = new NativeSearchQueryBuilder();
		// 添加基本分词查询
		queryBuilder.withQuery(builder);
		// 搜索，获取结果
		Page<CarEla> items = carRepository.search(queryBuilder.build());
		// 总条数
		long total = items.getTotalElements();
		System.out.println("total = " + total);
//		items.forEach(item -> System.out.println("item = " + JsonUtil.toJsonString(item)));
	}

	private List<String> getKeysName() {
		List<String> keyName = new ArrayList<>();
		keyName.add("brand");
		keyName.add("colour");
		keyName.add("model");
		return keyName;
	}

	private List<String> getKeysKeywordName() {
		List<String> keyName = new ArrayList<>();
		keyName.add("brand.keyword");
		keyName.add("colour.keyword");
		keyName.add("model.keyword");
		return keyName;
	}
}
