# ElasticSearch

#### 介绍
本Demo 是SpringBoot项目, 通过ElasticSearch + IK中文分词器 + Logstash数据同步来实现中文组合词语搜索。


#### CSDN 博客地址
[https://blog.csdn.net/Hyeshenghuo/article/details/98470272](https://blog.csdn.net/Hyeshenghuo/article/details/98470272)
#### 个人博客
[http://www.ydlb.xyz](http://www.ydlb.xyz)