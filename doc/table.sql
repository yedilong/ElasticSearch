CREATE TABLE `car` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `brand` varchar(20) DEFAULT '' COMMENT '品牌',
  `model` varchar(20) NOT NULL DEFAULT '' COMMENT '车型',
  `colour` varchar(10) NOT NULL COMMENT '颜色',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COMMENT='车辆基本信息';

INSERT INTO `car` (`id`, `brand`, `model`, `colour`)
VALUES
	(24, '大众', '新桑塔纳', '白色'),
	(25, '大众', '新桑塔纳', '银色'),
	(26, '大众', '新桑塔纳', '白色'),
	(27, '大众', '新桑塔纳', '白色'),
	(28, '大众', '新桑塔纳', '白色'),
	(29, '大众', '新桑塔纳', '白色'),
	(30, '大众', '新桑塔纳', '白色'),
	(31, '大众', '新桑塔纳', '白色'),
	(32, '大众', '新桑塔纳', '白色'),
	(33, '大众', '朗逸', '灰色'),
	(34, '大众', '朗逸', '灰色'),
	(35, '大众', '朗逸', '灰色'),
	(36, '大众', '朗逸', '灰色'),
	(37, '大众', '朗逸', '灰色'),
	(38, '大通', 'G10', '白色'),
	(39, '大通', 'G11', '白色'),
	(40, '大通', 'G12', '白色'),
	(41, '大通', 'G13', '白色'),
	(42, '大通', 'G14', '白色'),
	(43, '奥迪', 'A6L', '黑色'),
	(44, '奥迪', 'A6L', '黑色'),
	(45, '奥迪', 'A6L', '黑色'),
	(46, '大众', '新桑塔纳', '白色'),
	(47, '大众', '新桑塔纳', '白色');
